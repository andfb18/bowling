class ScoresController < ApplicationController
  before_action :set_score, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token  

  # GET /scores
  # GET /scores.json
  def index
    @scores = Score.all
  end

  # GET /scores/1
  # GET /scores/1.json
  def show
  end

  def calculate
    score = params[:score][:score_text]

    total_score = []
    turn_counts = 0

    score = score.downcase.split('')
    score.each_with_index do |turn, v|

      turn_counts += turn == 'x' ? 2 : 1
      next if turn_counts > 20

      if turn == 'x'
        actual_score = 10
        if ['x','/'].include?(score[v+2])
          actual_score += 10 if score[v+1] == 'x'
          actual_score += 10
          total_score << actual_score
          next
        else
          if score[v+1] == 'x' 
            actual_score += 10
            actual_score += (score[v+2] == '-' ? 0 : score[v+2].to_i)
          else
            actual_score += (score[v+1] == '-' ? 0 : score[v+1].to_i) + (score[v+2] == '-' ? 0 : score[v+2].to_i)
          end
          total_score << actual_score
          next
        end

      elsif turn == '/'
        next
      elsif turn == '-'
        next
      else
        actual_score = 0
        if score[v+1] == '/'
          actual_score = 10
          if score[v+2] == 'x'
            actual_score += 10
          else
            actual_score += (score[v+2] == '-' ? 0 : score[v+2].to_i)
          end
        else
          actual_score += (turn == '-' ? 0 : turn.to_i)
        end
        total_score << actual_score
        next
      end
    end

    total = total_score.inject(0){|sum,x| sum + x }

    Score.create({total: total})
    redirect_to scores_path, notice: 'Score was successfully calculated.'
  end

  # GET /scores/new
  def new
    @score = Score.new
  end

  # GET /scores/1/edit
  def edit
  end

  # POST /scores
  # POST /scores.json
  def create
    @score = Score.new(score_params)

    respond_to do |format|
      if @score.save
        format.html { redirect_to @score, notice: 'Score was successfully created.' }
        format.json { render :show, status: :created, location: @score }
      else
        format.html { render :new }
        format.json { render json: @score.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /scores/1
  # PATCH/PUT /scores/1.json
  def update
    respond_to do |format|
      if @score.update(score_params)
        format.html { redirect_to @score, notice: 'Score was successfully updated.' }
        format.json { render :show, status: :ok, location: @score }
      else
        format.html { render :edit }
        format.json { render json: @score.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scores/1
  # DELETE /scores/1.json
  def destroy
    @score.destroy
    respond_to do |format|
      format.html { redirect_to scores_url, notice: 'Score was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_score
      @score = Score.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def score_params
      params.fetch(:score, {})
    end
end
